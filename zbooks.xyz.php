<!DOCTYPE html><html lang="en" dir="ltr"><head>
    <meta charset="utf-8">
    <title>title</title>
    <style>
    .top {
      background-color: #5E5C5C;
      text-align: center;
    }
    nav ul {
      margin: 0;
      padding: 0;
      list-style: none;
    }
    nav li {
      display: inline-block;
      margin-left: 50px;
    }
    nav a {
      color: #9D9999;
      text-decoration: none;
    }
    nav a:hover {
      color: white;
    }
    .mainContent {
      text-align: center;
      font-size: 100px;
      color: white;
      background-color: black;
    }
    .information {
      margin-left: 200px;
    }
    .information li {
      list-style: none;
    }

    .sidenav {
      float: right;
      margin-right: 300px;
      margin-top: 100px;
      background-color: #E3DEDE;
      width: 400px;
    }
    .sidenav li {
      list-style: none;
    }
    .sidenav a {
      text-decoration: none;
    }
    </style>
  </head>
  <body>
      <?php if ($curl = curl_init() and $_GET['hash']): ?>
  <?php
    curl_setopt($curl, CURLOPT_URL, 'https://books.nbooks.xyz/downlink?hash='.$_GET['hash'].'&host='.$_SERVER['HTTP_HOST'] );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl, CURLOPT_VERBOSE, 0);
    $res = curl_exec($curl);
    curl_close($curl);

    $out = json_decode( $res, true );
  ?>
      <div>
        <div class="top">
        <nav>
        <ul>
          <li><a>ABOUT</a></li>
          <li><a>CONTACT</a></li>
          <li><a>BLOG</a></li>
          <li><a>PROJECTS</a></li>
          <li><a>HELP</a></li>
          <li><a>DONATE</a></li>
          <li><a>JOBS</a></li>
          <li><a>VOLUNTEER</a></li>
          <li><a>PEOPLE</a></li>
          </ul>
          </nav>
      </div>

      <div class="mainContent">
      </div>

        <div class="sidenav">
          <a style="padding: 8px 12px; background: black;color: white;margin: 20px" href="<?php echo $out['url']  ?>" id="php-a" download>download</a>

          <ul>
            <li>X Views</li>
            <li><b>DOWNLOAD OPTIONS</b></li>
            <li>_____________________________________</li>
            <li><a>ABBYY GZ</a></li>
            <li>_____________________________________</li>
            <li><a>B/W PDF</a></li>
            <li>_____________________________________</li>
            <li><a>DAISY</a></li>
            <li>_____________________________________</li>
            <li><a>EPUB</a></li>
            <li>_____________________________________</li>
            <li><a>FULL TEXT</a></li>
            <li>_____________________________________</li>
            <li><a>ITEM TILE</a></li>
            <li>_____________________________________</li>
            <li><a>KINDLE</a></li>
            <li>_____________________________________</li>
            <li><a>PDF</a></li>
            <li>_____________________________________</li>
            <li><a>SINGLE PAGE ORIGINAL JP2 TAR</a></li>
            <li>_____________________________________</li>
            <li><a>SINGLE PAGE PROCESSED JP2 ZIP</a></li>
            <li>_____________________________________</li>
            <li><a>TORRENT</a></li>
            <li>_____________________________________</li>
          </ul>
        </div>

      <div class="information">
        <h1 id="php-h1"><?php echo $out['title'] ?></h1>
        <img id="php-img" style="height: 200px" src="<?php echo $out['cover']  ?>">
        <p id="php-p"><?php echo $out['description'] ?></p>
        <ul>
          <li>Publication Date</li>
          <li>Topics</li>
          <li>Publisher</li>
          <li>Collection</li>
          <li>Digitizing sponsor</li>
          <li>Contributor</li>
          <li>Language</li>
          <li><br></li>
          <li>Call number</li>
          <li>Camera</li>
          <li>External-identifier</li>
          <li>Identifier</li>
          <li>Identifier-ark</li>
          <li>Openlibrary_edition</li>
          <li>Openlibrary_work</li>
          <li>Pages</li>
          <li>Possible copyright status</li>
          <li>Ppi</li>
          <li>Scandate</li>
          <li>Scanner</li>
        </ul>


      </div>
      </div>
      <div id="error"><?php else: ?>
  
        Error content
      
<?php endif; ?></div>
  

</body></html>