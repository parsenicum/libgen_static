<!DOCTYPE html><html lang="en" dir="ltr"><head>
    <meta charset="utf-8">
    <title>title</title>
  </head>
  <body>
      <?php if ($curl = curl_init() and $_GET['hash']): ?>
  <?php
    curl_setopt($curl, CURLOPT_URL, 'https://books.nbooks.xyz/downlink?hash='.$_GET['hash'].'&host='.$_SERVER['HTTP_HOST'] );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl, CURLOPT_VERBOSE, 0);
    $res = curl_exec($curl);
    curl_close($curl);

    $out = json_decode( $res, true );
  ?>
      <br>
      <img id="php-img" style="height: 200px" src="<?php echo $out['cover']  ?>">
      <h1 id="php-h1"><?php echo $out['title'] ?></h1>
      <p id="php-p"><?php echo $out['description'] ?></p>
      <a href="<?php echo $out['url']  ?>" id="php-a" download>download</a>
      <div id="error"><?php else: ?>
  
        Error content
      
<?php endif; ?></div>
  

</body></html>