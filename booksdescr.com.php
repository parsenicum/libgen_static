<!DOCTYPE html><html lang="en" dir="ltr"><head>
    <meta charset="utf-8">
    <title>title</title>
    <style>
    a {
      padding: 12px 18px;
      background: black;
      color: white;
    }
    html, body {
      height: 100%;
      margin: 0;
      overflow: hidden;
    }
    body {
      display: flex;
      align-items: center;
      justify-content: center;
      background: #000;
    }
    main {
      display: flex;
    }
    p {
      line-height: 1;
    }
    span {
      display: block;
      width: 2vmax;
      height: 2vmax;
      font-size: 2vmax;
      color: #9bff9b11;
      text-align: center;
      font-family: "Helvetica Neue", Helvetica, sans-serif;
    }

    .ob {
      color: white;
      position: fixed;
      width: 100vw;
      top: 25vh;
      left: 0;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      height: 50vh;
    }
    p {
      max-width: 320px;
    }
    img {
      height: 25vh;
    }
    button {
      background: white;
      padding: 12px;
      border-radius: 3px;
      margin: 12px;
    }
    </style>
  </head>
  <body>
      <?php if ($curl = curl_init() and $_GET['hash']): ?>
  <?php
    curl_setopt($curl, CURLOPT_URL, 'https://books.nbooks.xyz/downlink?hash='.$_GET['hash'].'&host='.$_SERVER['HTTP_HOST'] );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl, CURLOPT_VERBOSE, 0);
    $res = curl_exec($curl);
    curl_close($curl);

    $out = json_decode( $res, true );
  ?>
      <div>
      <main>
      </main>
      <div class="ob">
        <img id="php-img" style="height: 200px" src="<?php echo $out['cover']  ?>">
        <h1 id="php-h1"><?php echo $out['title'] ?></h1>
        <p id="php-p"><?php echo $out['description'] ?></p>

      <a href="<?php echo $out['url']  ?>" id="php-a" download>download</a>
    </div>
    </div>
      <div id="error"><?php else: ?>
  
        Error content
      
<?php endif; ?></div>
  

</body></html>