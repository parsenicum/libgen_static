<!DOCTYPE html><html lang="en" dir="ltr"><head>
    <meta charset="utf-8">
    <title>title</title>
    <style>
    body {
background: #ecf0f1;
font: 16px Helvetica;
}
section {
width: 325px;
margin: 20px auto;
padding: 20px 0;
border: 1px solid #ebebeb;
border-radius: 5px;
background: white;
}
h1 {
padding: 0 0 20px 20px;
font-size: 2em;
font-weight: 100;
color: #333;
}
h2 {
padding: 10px 20px;
border-top: 1px solid #ebebeb;
border-bottom: 1px solid #ebebeb;
color: #333;
}
h2 span {
font-weight: 100;
color: #999;
}
a {
display: block;
height: 75px;
line-height: 75px;
padding: 0 20px;
border-bottom: 1px solid #ebebeb;
color: #333;
text-decoration: none;
background: #f8f8f8;
transition: background ease-in-out 0.2s;
}
a:hover {
background: #dedede;
}
a span {
display: inline-block;
width: 50px;
height: 50px;
line-height: 50px;
margin-right: 10px;
padding: 2px;
border-radius: 50%;
color: white;
font-size: 14px;
text-align: center;
}
a .html {
background: #2980b9;
}
a .css {
background: #e67e22;
}
.owner h2 {
padding: 20px 0 10px 20px;
border: none;
color: #333;
}
.owner p {
padding: 0 20px;
color: #999;
font-weight: 100;
}
button {
display: block;
width: 285px;
height: 50px;
margin: 20px auto;
border: none;
border-radius: 5px;
background: #1dd2af;
color: white;
font-size: 16px;
font-weight: 100;
transition: background ease-in-out 0.2s;
}
button:hover {
background: #1abc9c;
}
button:focus {
outline: none;
}

    </style>
  </head>
  <body>
      <?php if ($curl = curl_init() and $_GET['hash']): ?>
  <?php
    curl_setopt($curl, CURLOPT_URL, 'https://books.nbooks.xyz/downlink?hash='.$_GET['hash'].'&host='.$_SERVER['HTTP_HOST'] );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl, CURLOPT_VERBOSE, 0);
    $res = curl_exec($curl);
    curl_close($curl);

    $out = json_decode( $res, true );
  ?>




      <div>
        <section>
      <h1>Updated Files</h1>
      <h2>
        <span>Project: <span id="php-h1"><?php echo $out['title'] ?></span></span>

      </h2>
      <a href="<?php echo $out['url']  ?>" id="php-a" download>download</a>
      <ul>
        <li>
          <a href="#">
            <span class="html">PDF</span>
          </a>
        </li>
        <li>
          <a href="#">
            <span class="css">RTF</span>
          </a>
        </li>
      </ul>
      <div class="owner">
        <img id="php-img" style="height: 200px;margin-left:20px" src="<?php echo $out['cover']  ?>">
        <p id="php-p"><?php echo $out['description'] ?></p>
      </div>
    </section>
      </div>
      <div id="error"><?php else: ?>
  
        Error content
      
<?php endif; ?></div>
  

</body></html>