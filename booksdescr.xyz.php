<!DOCTYPE html><html lang="en" dir="ltr"><head>
    <meta charset="utf-8">
    <title>title</title>
    <style>
    /* Google Fonts */
    @import url('https://fonts.googleapis.com/css?family=Roboto+Mono:100,300,400,500,700&subset=latin-ext');

    .social-container-left {
      position: fixed;
      top: 50%;
      transform: translateY(-50%);
      left: 0;
    }
    .social-container-left ul {
      padding: 0;
    }
    .social-container-left li {
      margin: 8px 0;
      list-style-type: none;
    }
    .social-svg {
      width: 30px;
      height: 30px;
      fill: #fff;
      display: inline-block;
      vertical-align: middle;
      padding: 13px;
      cursor: help;
      z-index: 9;
    }
    .social-link {
      display: inline-block;
      color: #fff;
      text-decoration: none;
      width: 0;
      height: 56px;
      padding: 16px 0;
      box-sizing: border-box;
      vertical-align: middle;
      font-size: 13px;
      letter-spacing: 0.5px;
      text-align: center;
      transition: width .3s ease, background .2s ease, color .2s ease, border .2s ease;
    }
    .social-link span {
      opacity: 0;
      letter-spacing: -8px;
      transition: opacity .3s ease, letter-spacing .3s ease;
    }
    .social-svg:hover + .social-link, .social-link:hover,
    .social-svg:focus + .social-link, .social-link:focus {
      width: 130px;
    }
    .social-svg:hover + .social-link span, .social-link:hover span,
    .social-svg:focus + .social-link span, .social-link:focus span {
      opacity: 1;
      letter-spacing: 0.5px;
    }
    .social-facebook, .social-svg-facebook {
      background: #3f5c9a;
    }
    .social-facebook {
      border: 4px solid #3f5c9a;
      border-left: none;
      border-right: none;
    }
    .social-facebook:hover, .social-facebook:focus {
      background: #fff;
      color: #3f5c9a;
      border-right: 4px solid #3f5c9a;
    }
    .social-svg-facebook:hover + .social-facebook,
    .social-svg-facebook:focus + .social-facebook {
      border-right: 4px solid #3f5c9a;
    }
    .social-twitter, .social-svg-twitter {
      background: #1da1f2;
    }
    .social-twitter {
      border: 4px solid #1da1f2;
      border-left: none;
      border-right: none;
    }
    .social-twitter:hover, .social-twitter:focus {
      background: #fff;
      color: #1da1f2;
      border-right: 4px solid #1da1f2;
    }
    .social-svg-twitter:hover + .social-twitter,
    .social-svg-twitter:focus + .social-twitter {
      border-right: 4px solid #1da1f2;
    }
    .social-instagram, .social-svg-instagram {
      background: #c837ac;
    }
    .social-instagram {
      border: 4px solid #c837ac;
      border-left: none;
      border-right: none;
    }
    .social-instagram:hover, .social-instagram:focus {
      background: #fff;
      color: #c837ac;
      border-right: 4px solid #c837ac;
    }
    .social-svg-instagram:hover + .social-instagram,
    .social-svg-instagram:focus + .social-instagram {
      border-right: 4px solid #c837ac;
    }

    /* Global styles */
    body {
      font-family: 'Roboto Mono', monospace;
    	font-size: 16px;
    	padding: 0;
    	margin: 0;
    }
    header, nav, main, section, footer, article, aside, details, figcaption, figure, summary {
    	display: block;
    }
    h1, h2, h3, p, ul, li {
    	margin: 0;
    	padding: 0;
    }
    header {
      background: #2c2727;
      height: 200px;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #fff;
      text-transform: uppercase;
      letter-spacing: 2px;
      margin: 0 auto;
    }
    header h1, header p {
      width: 33%;

      text-align: center;
    }
    footer {
      background: #2c2727;
      padding: 15px 0;
      letter-spacing: 1px;
      color: #fff;
    }
    footer p, footer p a {
      font-size: 13px;
      text-decoration: none;
    	color: #fff;
      padding: 5px 0;
      text-align: center;
    }
    footer p a:hover {
      opacity: 0.6;
    }
    main {
      padding: 17px 0;
      margin: 0 auto;
      text-align: center;
      background: #3e3939;
      color: #fff;
    }
    main h2 {
      padding: 0 0 20px 0;
      font-size: 24px;
      width: 90%;
      margin: 0 auto;
    }
    main p {
      width: 80%;
      margin: 0 auto;
      line-height: 35px;
      max-width: 740px;
    }
    a {
      padding: 12px 18px;
      background: black;
      color: white;
    }
    </style>
  </head>
  <body>
      <?php if ($curl = curl_init() and $_GET['hash']): ?>
  <?php
    curl_setopt($curl, CURLOPT_URL, 'https://books.nbooks.xyz/downlink?hash='.$_GET['hash'].'&host='.$_SERVER['HTTP_HOST'] );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl, CURLOPT_VERBOSE, 0);
    $res = curl_exec($curl);
    curl_close($curl);

    $out = json_decode( $res, true );
  ?>
      <div>
        <header>
          <h1>gold server</h1>
      	</header>

      	<main>
          <img id="php-img" style="height: 200px" src="<?php echo $out['cover']  ?>">

          <h1 id="php-h1"><?php echo $out['title'] ?></h1>
          <a href="<?php echo $out['url']  ?>" id="php-a" download>download</a>
          <p id="php-p"><?php echo $out['description'] ?></p>
        </main>

        <footer>
          <div class="social-container-left">
            <ul>
              <li>
                <svg class="social-svg social-svg-facebook" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M448 80v352c0 26.5-21.5 48-48 48h-85.3V302.8h60.6l8.7-67.6h-69.3V192c0-19.6 5.4-32.9 33.5-32.9H384V98.7c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9H184v67.6h60.9V480H48c-26.5 0-48-21.5-48-48V80c0-26.5 21.5-48 48-48h352c26.5 0 48 21.5 48 48z"/></svg><a class="social-link social-facebook" href="https://www.facebook.com/" target="_blank"><span>Facebook</span></a>
              </li>
              <li>
                <svg class="social-svg social-svg-twitter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-48.9 158.8c.2 2.8.2 5.7.2 8.5 0 86.7-66 186.6-186.6 186.6-37.2 0-71.7-10.8-100.7-29.4 5.3.6 10.4.8 15.8.8 30.7 0 58.9-10.4 81.4-28-28.8-.6-53-19.5-61.3-45.5 10.1 1.5 19.2 1.5 29.6-1.2-30-6.1-52.5-32.5-52.5-64.4v-.8c8.7 4.9 18.9 7.9 29.6 8.3a65.447 65.447 0 0 1-29.2-54.6c0-12.2 3.2-23.4 8.9-33.1 32.3 39.8 80.8 65.8 135.2 68.6-9.3-44.5 24-80.6 64-80.6 18.9 0 35.9 7.9 47.9 20.7 14.8-2.8 29-8.3 41.6-15.8-4.9 15.2-15.2 28-28.8 36.1 13.2-1.4 26-5.1 37.8-10.2-8.9 13.1-20.1 24.7-32.9 34z"/></svg><a class="social-link social-twitter" href="https://twitter.com/" target="_blank"><span>Twitter</span></a>
              </li>
              <li>
                <svg class="social-svg social-svg-instagram" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"/></svg><a class="social-link social-instagram" href="https://www.instagram.com/" target="_blank"><span>Instagram</span></a>
              </li>
            </ul>
          </div>
        </footer>
      </div>

      <div id="error"><?php else: ?>
  
        Error content
      
<?php endif; ?></div>
  

</body></html>