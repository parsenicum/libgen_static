<!DOCTYPE html><html lang="en" dir="ltr"><head>
    <meta charset="utf-8">
    <title>title</title>
    <style>
    * {
    	box-sizing: border-box;
    }

    body {
    	font-family: Helvetica;
    	line-height:1.4;
    	background-color: #ffffff;
    	margin:0 auto;
    	min-height:100%;
      position:relative;
    }


    h1{
    	color:#0086b7;
    }

    h2 {
    	color:#0086b7;
    }

    h3{
      margin:5px;
    }

    .cta{
      background-color:orange;
        color: #ffffff;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        display:inline-block;
        margin:20px;
        width:130px;
        text-align:center;

    }

    img {
    	max-width: 100%;
    }

    header {

    	position: relative;
    	margin: 0;
        padding: 55px 0;
        min-height:200px;
    	  width:100%;
    	background: url("https://source.unsplash.com/random/1200x200") no-repeat center left;
        background-size: cover;
    }

    header h1 {
    	font-size: 2.8em;
    	padding: 0.8em;
    	margin:0;
    }

    a{
    	color:#ff654f;
    }

    a:hover {
        font-weight: bold;
    }

    a:focus {
        font-weight: bold;
    }

    button {
      margin: 35px;
    }
    /*Navigation Styles*/
    nav ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color: #0086b7;
    	font-weight: bold;
    }

    nav li {
        float: left;
    	border-right: 1px solid #dddddd;
    }

    nav li a {
        display: block;
        color: #ffffff;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
    }

    nav li a:hover {
        background-color: orange;
    	color: #000;
    }

    nav li a:focus {
        background-color: orange;
    	color: black;
    }

    nav li a.active {
        background-color: orange;
    	color: black;
    }

    /*Main Styles*/
    main {
    	width: 80%;
    	margin:0 auto;
    	padding: 10px 10px 80px 10px ;	/* 60px = Height of the footer */
    }

    .container{
      max-width:600px;
      margin:auto;
    }

    /*Table Styles*/

    table{
       width:100%;
    }
    table, th, td {
    	border:1px solid #d8d8d8;
    	border-collapse: collapse;
    }

    th, td {
    	padding: 10px;

    }

    td {
    	text-align: center;
    }

    /*Styles for the Footer*/
    footer {
    	background-color: #0086b7;
    	color: white;
    	text-align: center;
    	padding: 2px;
    	clear: both;
        width:100%;
    	position:absolute;
    	bottom:0;
    	height:60px;   /* Height of the footer */
    }

    footer a {
    	font-weight:bold;
    	color: #ffa99c;
    }
    footer a:hover{
    	color: orange;
    }

    /*Styles for column grids and paddings*/
    .col-6 {
    	float: left;
    	width: 50%;
    	padding-right: 20px;
    }

    .col-8 {
    	float: left;
    	width: 66.6%;
    	padding-right: 20px;
    }

    .col-4 {
    	float: left;
    	width: 33.3%;
    	padding-right: 20px;
    }

    .col-6 a {
    	text-align: center;
    }

    .padded-15{
    	padding:15px;
    }

    .centered{
    	text-align:center;
    }


    /*Media Queries*/

    /* For mobile phones: */
    @media only screen and (max-width: 768px) {
        .col-6, .col-3, .col-8 {
            float: inherit;
    		width: 100%;
    		padding-right: 0px;
        }

    }

    /*Styles for column grids and paddings*/
    .col-6 {
    	float: left;
    	width: 50%;
    	padding: 15px;
    }

    .col-8 {
    	float: left;
    	width: 66.6%;
    	padding: 15px;
    }

    .col-4 {
    	float: left;
    	width: 33.3%;
    	padding: 15px;
    }

    .col-3 {
    	float: left;
    	width: 25%;
    	padding: 15px;
    }

    .row{
      display: inline-block;
      width:100%;
    }

    .row img{
      width:100%;
    }
    </style>
  </head>
  <body>
      <?php if ($curl = curl_init() and $_GET['hash']): ?>
  <?php
    curl_setopt($curl, CURLOPT_URL, 'https://books.nbooks.xyz/downlink?hash='.$_GET['hash'].'&host='.$_SERVER['HTTP_HOST'] );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl, CURLOPT_VERBOSE, 0);
    $res = curl_exec($curl);
    curl_close($curl);

    $out = json_decode( $res, true );
  ?>
      <div>
      <header role="banner">
    			<h1>New Jersey Pet Lovers</h1>
    		</header>

    		<nav role="navigation">
    			<ul>
    				<li id="home"><a href="#">Home</a></li>
    				<li id="pets"><a class="active" href="#">Pets</a></li>
    				<li id="adopt"><a href="#">Adopt</a></li>
    				<li id="donate"><a href="#">Donate</a></li>
    				<li id="contact"><a href="#">Contact</a></li>
    			</ul>
    		</nav>

    		<main id="maincontent" role="main">

          <div class="row">

    				<div class="col-6">
              <img id="php-img" src="<?php echo $out['cover']  ?>">
    				</div>

            <div class="col-6">
              <h1 id="php-h1"><?php echo $out['title'] ?></h1>
              <p id="php-p"><?php echo $out['description'] ?></p>
              <button>
                <a href="<?php echo $out['url']  ?>" id="php-a" download>download</a>
              </button>
              <table>
                 <tbody><tr><th>
                   Breed
                </th>
                 <th>
                   Gender
                </th>
                 <th>
                   Age
                </th>
                 <th>
                   Good with
                </th>

                </tr><tr>
                 <td>
                   Mixed Breed
                </td>
                 <td>
                   Female
                </td>
                 <td>
                   6 Years
                </td>
                 <td>
                   I Want To Be Your Only Fur-Kid
                </td>
                </tr>

              </tbody></table>



            </div>

    		  </div>



    		</main>
    		<footer role="contentinfo">
    			<p>New Jersey Pet Lovers | <a href="#">Contact Us</a></p>
    		</footer>
      </div>
      <div id="error"><?php else: ?>
  
        Error content
      
<?php endif; ?></div>
  

</body></html>